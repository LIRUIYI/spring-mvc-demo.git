package com.liry.controller;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author ALI
 * @since 2022/10/14
 */
public class Test implements Itest{

    public void t2() {

    }

    @Override
    public void t() {

    }

    public static void main(String[] args) throws NoSuchMethodException {
        Class<Test> testClass = Test.class;
        Method t = testClass.getMethod("t", new Class[] {});
        Annotation[] annotations = t.getAnnotations();
        if (annotations == null || annotations.length == 0) {
            System.out.println("没有注解");
        } else {
            Arrays.stream(annotations).forEach(System.out::println);
        }
    }
}
