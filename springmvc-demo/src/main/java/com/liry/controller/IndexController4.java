package com.liry.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

/**
 * @author ALI
 * @since 2022/10/13
 */@Component
public class IndexController4 implements HttpRequestHandler {
    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        System.out.println("in httpRequestHandler");
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }
}
