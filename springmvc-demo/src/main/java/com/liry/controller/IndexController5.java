package com.liry.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ALI
 * @since 2022/10/13
 */
@Controller
@RequestMapping
public class IndexController5 implements Itest{

    @RequestMapping("/index5.html")
    public String index() {
        return "index";
    }

    @Override
    public void t() {

    }
}
