package com.liry.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ALI
 * @since 2022/10/3
 */
@Controller
@RequestMapping
public class IndexController {

    @GetMapping("/index.html")
    public String index() {
        return "index";
    }
}
