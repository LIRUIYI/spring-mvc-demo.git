package com.liry.controller;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author ALI
 * @since 2022/10/14
 */
public interface Itest {

    @GetMapping("/test.html")
    public void t();
}
