package com.liry.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * @author ALI
 * @since 2022/10/13
 */
@Component("/index3.html")
public class IndexController3 implements Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("in controller");
        ModelAndView result = new ModelAndView();
        result.setViewName("index");
        return result;
    }
}
