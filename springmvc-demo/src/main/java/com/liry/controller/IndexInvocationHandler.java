package com.liry.controller;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * @author ALI
 * @since 2022/10/14
 */
public class IndexInvocationHandler implements InvocationHandler {

    private Object target;

    public IndexInvocationHandler(Object target, Method method, Object[] objects) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object obj = method.invoke(target);
        return obj;
    }
}
