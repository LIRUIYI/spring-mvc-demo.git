package com.liry.controller;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

/**
 * @author ALI
 * @since 2022/10/12
 */
public class IndexController2 extends HttpServlet {
    private static final long serialVersionUID = -2964194399437247271L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("in httpServlet " + req.getMethod());
        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }
}
