package com.liry.custommvc.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.stereotype.Controller;

/**
 * 前端控制器
 *
 * @author ALI
 * @since 2022/10/5
 */
@Target({ElementType.TYPE})
@Documented
@Controller
@ResponseBody
@Retention(RetentionPolicy.RUNTIME)
public @interface RestController {

    String value() default "";
}
