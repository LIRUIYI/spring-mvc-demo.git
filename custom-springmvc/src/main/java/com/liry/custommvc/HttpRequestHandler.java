package com.liry.custommvc;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author ALI
 * @since 2022/10/5
 */
public class HttpRequestHandler {

    private Class<?> obj;

    private String beanName;

    private String url;

    private Method method;

    private List<String> paramNameList;

    private boolean returnView;

    public boolean isReturnView() {
        return returnView;
    }

    public void setReturnView(boolean returnView) {
        this.returnView = returnView;
    }

    public Class<?> getObj() {
        return obj;
    }

    public void setObj(Class<?> obj) {
        this.obj = obj;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public List<String> getParamNameList() {
        return paramNameList;
    }

    public void setParamNameList(List<String> paramNameList) {
        this.paramNameList = paramNameList;
    }
}
