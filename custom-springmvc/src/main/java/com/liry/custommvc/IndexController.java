package com.liry.custommvc;

import com.liry.custommvc.annotation.Controller;
import com.liry.custommvc.annotation.GetMapping;
import com.liry.custommvc.annotation.RequestMapping;
import com.liry.custommvc.annotation.RequestParam;
import com.liry.custommvc.annotation.ResponseBody;

/**
 * @author ALI
 * @since 2022/10/5
 */
@Controller
@RequestMapping
public class IndexController {

    @GetMapping("/index.html")
    public Object get() {
        return "dd.jsp";
    }

    @GetMapping("/index2.html")
    @ResponseBody
    public Object data() {
        return "ResponseBody data";
    }

    @GetMapping("/index3.html")
    @ResponseBody
    public Object data(String name) {
        return "name:" + name;
    }

    @GetMapping("/index4.html")
    @ResponseBody
    public Object name(@RequestParam("name") String name) {
        return "name:" + name;
    }

}
