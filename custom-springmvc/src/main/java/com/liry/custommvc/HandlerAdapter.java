package com.liry.custommvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 映射器处理器
 *
 * @author ALI
 * @since 2022/10/5
 */
public interface HandlerAdapter {

    boolean supports(Object controller);

    Object handle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception;
}
