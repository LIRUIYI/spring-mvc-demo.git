package com.liry.custommvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author ALI
 * @since 2022/10/5
 */
@Configuration
@ComponentScan("com.liry.custommvc")
public class CustomMvcAutoConfig {
}
