package com.liry.custommvc;

import javax.servlet.http.HttpServletRequest;

/**
 * 映射处理器
 *
 * @author ALI
 * @since 2022/10/5
 */
public interface HandlerMapping {

    /**
     * 获取处理器
     */
    HttpRequestHandler getHandler(HttpServletRequest request);
}
