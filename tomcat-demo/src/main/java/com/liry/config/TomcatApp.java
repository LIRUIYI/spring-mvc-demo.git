package com.liry.config;

import java.io.File;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

/**
 * @author ALI
 * @since 2022/10/3
 */
public class TomcatApp {
    public static void main(String[] args) throws LifecycleException {
        String path = System.getProperty("user.dir") + "/tomcat-demo";
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);
        Context context = tomcat.addWebapp("", new File(path + "/src/main/webapp").getAbsolutePath());
        //        StandardRoot standardRoot = new StandardRoot(context);
        //        standardRoot.addPreResources(new DirResourceSet(standardRoot, "/WEB-INF/classes", new File
        //        ("target/classes").getAbsolutePath(), "/"));
        //        context.setResources(standardRoot);
        tomcat.getConnector();
        tomcat.start();
        tomcat.getServer().await();
    }
}
